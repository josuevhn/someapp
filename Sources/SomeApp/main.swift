
let paco = Customer(name: "Paco", product: "Bitcoin", units: 2, price: 6000)

print(paco)

let olga = Customer(name: "Olga", product: "Ethereum", units: 5, price: 500)

print(olga)

if paco != olga {

	print("\(paco.name) and \(olga.name) aren't the same person.\n")

} // if
