
// MARK: - Customer Object
struct Customer: CustomStringConvertible {

   let name: String
   let product: String
   let units: Int
   let price: Int

   var description: String {

	return "\(name) bought \(units) \(product) at \(price) dollars each.\n"

   } // description

   static func ==(lhs: Customer, rhs: Customer) -> Bool {

	guard lhs.name == rhs.name else {

		return false

	} // guard

	guard lhs.units == rhs.units else {

		return false

	} // guard

	guard lhs.product == rhs.product else {

		return false

	} // guard

	guard lhs.price == rhs.price else {

		return false

	} // guard

	return true

   } // func ==

   static func !=(lhs: Customer, rhs: Customer) -> Bool {

	guard lhs.name != rhs.name else {

		return false

	} // guard

	guard lhs.units != rhs.units else {

		return false

	} // guard

	guard lhs.product != rhs.product else {

		return false

	} // guard

	guard lhs.price != rhs.price else {

		return false

	} // guard

	return true

   } // func !=

} // Customer
