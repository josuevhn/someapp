# How to contribute

Third-party patches are essential for keeping SomeApp great. We simply can't
access the huge number of platforms and myriad configurations for running
SomeApp. We want to keep it as easy as possible to contribute changes that
get things working in your environment...
